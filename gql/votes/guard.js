const { shield } = require("graphql-shield");
const { isAuthenticated } = require("../authorization-gates");


const guard = shield({
  Mutation: {
    vote: isAuthenticated,
    unvote: isAuthenticated,
  }
});

module.exports = { guard };