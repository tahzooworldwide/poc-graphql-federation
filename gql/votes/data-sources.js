const { MongoDataSource } = require('apollo-datasource-mongodb');

class VoteDataSource extends MongoDataSource {
  async getVotesForIdea(id) {
    const query = { ideaId: id };
    const opts = { sort: { createdAt: 1 } }
    return await this.collection.find(query, opts).toArray();
  }
  async getVoteCountForIdea(id) {
    const query = { ideaId: id };
    return await this.collection.count(query);
  }
  async getVotesByUser(id) {
    const query = { userId: id };
    const opts = { sort: { createdAt: -1 } }
    return await this.collection.find(query, opts).toArray();
  }
  async getVoteCountByUser(id) {
    const query = { userId: id };
    return await this.collection.count(query);
  }
  async vote(ideaId, userId) {
    const count = await this.collection.count({ ideaId, userId });
    if (count > 0) {
      return false;
    }
    const propsedRecored = {
      ideaId,
      userId,
      createdAt: (new Date()).getTime()
    }
    const result = await this.collection.insertOne(propsedRecored);
    return result.insertedId ? true : false;
  }
  async unvote(ideaId, userId) {
    const count = this.collection.count({ ideaId, userId });
    if (count === 0) {
      return false;
    }
    const result = await this.collection.deleteOne({ ideaId, userId });
    return result.deletedCount === 1;
  }
}

module.exports = { VoteDataSource }