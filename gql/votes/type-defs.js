const { gql } = require('apollo-server');

const typeDefs = gql`
  type Vote {
    _id: ID!
    userId: Int!
    user: User
    ideaId: Int!
    idea: Idea
    createdAt: Float!
  }

  input VoteInput {
    ideaId: Int!
    userId: Int!
  }

  extend type User @key(fields: "id") {
    id: Int! @external
    votes: [Vote]
    voteCount: Int
  }

  extend type Idea @key(fields: "id") {
    id: Int! @external
    votes: [Vote]
    voteCount: Int
  }

  extend type Mutation {
    vote(input: VoteInput): Boolean!
    unvote(input: VoteInput): Boolean!
  }
`;

module.exports = {
  typeDefs
};