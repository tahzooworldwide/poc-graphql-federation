const resolvers = {
  Vote: {
    user(vote) {
      return { __typename: "User", id: vote.userId };
    },
    idea(vote) {
      return { __typename: "Idea", id: vote.ideaId };
    }
  },
  User: {
    votes: async ({ id }, _, { dataSources }) => {
      return await dataSources.vote.getVotesByUser(id);
    },
    voteCount: async ({ id }, _, { dataSources }) => {
      return await dataSources.vote.getVoteCountByUser(id);
    },
  },
  Idea: {
    votes: async ({ id }, _, { dataSources }) => {
      return await dataSources.vote.getVotesForIdea(id);
    },
    voteCount: async ({ id }, _, { dataSources }) => {
      return await dataSources.vote.getVoteCountForIdea(id);
    },
  },
  Mutation: {
    async vote(_, { input: { ideaId, userId } }, { dataSources }) {
      return await dataSources.vote.vote(ideaId, userId);
    },
    async unvote(_, { input: { ideaId, userId } }, { dataSources }) {
      return await dataSources.vote.unvote(ideaId, userId);
    }
  }
}


const referenceResolvers = null;


module.exports = {
  resolvers,
  referenceResolvers
}