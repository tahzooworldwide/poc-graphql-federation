const { shield } = require("graphql-shield");
const { isAuthenticated } = require("../authorization-gates");


const guard = shield({
  Mutation: {
    createComment: isAuthenticated,
  }
});

module.exports = { guard };