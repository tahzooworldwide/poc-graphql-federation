const resolvers = {
  Comment: {
    user(comment) {
      return { __typename: "User", id: comment.userId };
    },
    idea(comment) {
      return { __typename: "Idea", id: comment.ideaId };
    }
  },
  User: {
    comments: async ({ id }, _, { dataSources }) => {
      return await dataSources.comment.getCommentsByUser(id);
    },
    commentCount: async ({ id }, _, { dataSources }) => {
      return await dataSources.comment.getCommentCountForUser(id);
    },
  },
  Idea: {
    comments: async ({ id }, _, { dataSources }) => {
      return await dataSources.comment.getCommentsByIdea(id);
    },
    commentCount: async ({ id }, _, { dataSources }) => {
      return await dataSources.comment.getCommentCountForIdea(id);
    },
  },
  Mutation: {
    async createComment(_, { input: { ideaId, userId, body } }, { dataSources }) {
      return await dataSources.comment.createComment(ideaId, userId, body);
    },
  }
};



const referenceResolvers = null;


module.exports = {
  resolvers,
  referenceResolvers
}