const { gql } = require('apollo-server');

const typeDefs = gql`

  type Comment {
    _id: ID!
    id: Int!
    userId: Int!
    user: User
    ideaId: Int!
    idea: Idea
    body: String!
    createdAt: Float!
  }

  input CommentInput {
    ideaId: Int!
    userId: Int!
    body: String
  }

  extend type User @key(fields: "id") {
    id: Int! @external
    comments: [Comment]
    commentCount: Int
  }

  extend type Idea @key(fields: "id") {
    id: Int! @external
    comments: [Comment]
    commentCount: Int
  }

  extend type Mutation {
    createComment(input: CommentInput): Comment
  }
`;

module.exports = {
  typeDefs
};