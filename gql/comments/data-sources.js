const { MongoDataSource } = require('apollo-datasource-mongodb');

class CommentDataSource extends MongoDataSource {
  async nextId() {
    const result = await this.collection.find({}, { id: 1, _id: 0 }).sort({ id: -1 }).limit(1).toArray();
    console.log("nextId", result);
    return result.length ? parseInt(result[0].id + 1) : 1;
  }
  async getIdeas() {
    return await this.collection.find({}).toArray();
  }
  async getCommentsByIdea(id) {
    const query = { ideaId: id };
    const opts = { sort: { createdAt: 1 } }
    return await this.collection.find(query, opts).toArray();
  }

  async getCommentCountForIdea(id) {
    const query = { ideaId: id };
    return await this.collection.count(query);
  }
  async getCommentsByUser(id) {
    const query = { userId: id };
    const opts = { sort: { createdAt: -1 } }
    return await this.collection.find(query, opts).toArray();
  }
  async getCommentCountByUser(id) {
    const query = { userId: id };
    return await this.collection.count(query);
  }
  async createComment(ideaId, userId, body) {
    const id = await this.nextId();
    const proposedRecord = {
      id,
      ideaId,
      userId,
      body,
      createdAt: (new Date()).getTime()
    }

    const result = await this.collection.insertOne(proposedRecord);
    const record = await this.collection.findOne({ _id: result.insertedId });
    return record;
  }
}

module.exports = { CommentDataSource }