const { rule } = require('graphql-shield');

const isGuest = rule()((_, __, ctx) => {
  const result =  ctx.user === null;
  return result ? true : "You are already authenticated."
});
const isAuthenticated = rule()((_, __, ctx) => {
  return ctx.user !== null;
});

module.exports = { 
  isAuthenticated,
  isGuest
}