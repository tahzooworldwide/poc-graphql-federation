const { ApolloServer } = require('apollo-server');
const { buildFederatedSchema } = require('@apollo/federation');
const { applyMiddleware } = require('graphql-middleware');
const { addResolversToSchema } = require('apollo-graphql');
const { MongoClient } = require('mongodb');
const { UserDataSource } = require('./data-sources');
const { typeDefs } = require('./type-defs');
const { resolvers, referenceResolvers } = require('./resolvers');
const { guard } = require('./guard');

const client = new MongoClient(process.env.MONGO_HOST);
client.connect()

let schema = buildFederatedSchema([{ typeDefs, resolvers }]);
schema = applyMiddleware(schema, guard)
if (referenceResolvers) {
  addResolversToSchema(schema, referenceResolvers);
}

const server = new ApolloServer({
  schema,
  dataSources: () => {
    return {
      user: new UserDataSource(
        client
          .db(process.env.MONGO_DATABASE)
          .collection(process.env.MONGO_COLLECTION)
      )
    }
  },
  context: ({ req }) => {
    const user = req.headers.user ? JSON.parse(req.headers.user) : null;
    return { user };
  }
});

server.listen({ port: process.env.PORT }).then(({ url }) => {
  console.log(`Users GQL Server ready at ${url}`);
});

process.on('SIGINT', async () => {
  console.info('[SIGINT] Shutting Down.');
  await client.close();
  console.info('[SIGINT] Shutdown Complete.');
});

process.on('SIGTERM', async () => {
  console.info('[SIGTERM] Shutting Down.');
  await client.close();
  console.info('[SIGTERM] Shutdown Complete.');
});