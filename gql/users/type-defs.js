const { gql } = require('apollo-server');

const typeDefs = gql`
  type User @key(fields: "id"){
    _id: ID!
    id: Int!
    name: String!
    email: String!
    lastSeen: Float
    createdAt: Float!
    updatedAt: Float
  }

  type Query {
    users: [User!]!
    user(id: Int!): User
  }
`;

module.exports = {
  typeDefs
};