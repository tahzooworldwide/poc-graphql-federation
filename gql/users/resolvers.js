const resolvers = {
  Query: {
    users: async (_, __, { dataSources }) => {
      return await dataSources.user.getUsers();
    },
    user: async (_, { id }, { dataSources }) => {
      return await dataSources.user.getUser(id);
    }
  }
};

const referenceResolvers = {
  User: {
    __resolveReference: async (reference, { dataSources }) => {
      return await dataSources.user.getUser(reference.id)
    }
  },
};

module.exports = {
  resolvers,
  referenceResolvers
}