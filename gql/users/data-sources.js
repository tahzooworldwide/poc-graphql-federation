const { MongoDataSource } = require('apollo-datasource-mongodb');

class UserDataSource extends MongoDataSource {
  async getUsers() {
    return await this.collection.find({}).toArray();
  }
  async getUser(id) {
    return await this.collection.findOne({ id: id });
  }
}

module.exports = { UserDataSource }