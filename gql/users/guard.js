const { shield } = require("graphql-shield");
const { isAuthenticated } = require("../authorization-gates");


const guard = shield({
  Query: {
    users: isAuthenticated,
    user: isAuthenticated
  }
});

module.exports = { guard };