const { shield } = require("graphql-shield");
const { isAuthenticated } = require("../authorization-gates");


const guard = shield({
  Query: {
    ideas: isAuthenticated,
    idea: isAuthenticated
  },
  Mutation: {
    createIdea: isAuthenticated,
    updateIdea: isAuthenticated,
  }
});

module.exports = { guard };