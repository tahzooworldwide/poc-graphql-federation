const resolvers = {
  Idea: {
    user(idea) {
      return { __typename: "User", id: idea.userId };
    }
  },
  User: {
    ideas: async ({ id }, _, { dataSources }) => {
      return await dataSources.idea.getIdeasByUser(id);
    }
  },
  Query: {
    ideas: async (_, __, { dataSources }) => {
      return await dataSources.idea.getIdeas();
    },
    idea: async (_, { id }, { dataSources }) => {
      return await dataSources.idea.getIdea(id);
    }
  },
  Mutation: {
    async createIdea(_, { input: { userId, title, body } }, { dataSources }) {
      return await dataSources.idea.createIdea(userId, title, body);
    },
    async updateIdea(_, { id, input: { title, body } }, { dataSources }) {
      return await dataSources.idea.updateIdea(id, title, body);
    }
  }
};

const referenceResolvers = {
  Idea: {
    async __resolveReference(reference, { dataSources }) {
      return await dataSources.idea.getIdea(reference.id)
    },
  }
};

module.exports = {
  resolvers,
  referenceResolvers
}