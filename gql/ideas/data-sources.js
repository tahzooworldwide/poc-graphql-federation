const { MongoDataSource } = require('apollo-datasource-mongodb');

class IdeaDataSource extends MongoDataSource {
  async nextId() {
    const result = await this.collection.find({}, { id: 1, _id: 0 }).sort({ id: -1 }).limit(1).toArray();
    console.log("nextId", result);
    return result.length ? parseInt(result[0].id + 1) : 1;
  }
  async getIdeas() {
    return await this.collection.find({}).toArray();
  }
  async getIdea(id) {
    return await this.collection.findOne({ id: id });
  }
  async getIdeasByUser(id) {
    const query = { userId: id };
    const opts = { sort: { createdAt: -1 } }
    return await this.collection.find(query, opts).toArray();
  }
  async createIdea(userId, title, body) {
    const id = await this.nextId();
    const now = (new Date()).getTime();
    const proposedRecord = {
      id,
      userId,
      title,
      body,
      createdAt: now,
      updatedAt: now
    }

    const result = await this.collection.insertOne(proposedRecord);
    const record = await this.collection.findOne({ _id: result.insertedId });
    return record;
  }
  async updateIdea(id, title, body) {
    const updatedAt = (new Date()).getTime();
    const result = await this.collection.findOneAndUpdate({ id }, { $set: { title, body, updatedAt } });
    return result.value;
  }
}

module.exports = { IdeaDataSource }