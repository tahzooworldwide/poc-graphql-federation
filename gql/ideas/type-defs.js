const { gql } = require('apollo-server');

const typeDefs = gql`

  type Idea @key(fields: "id") {
    _id: ID!
    id: Int!
    userId: Int!
    user: User
    title: String!
    body: String
    createdAt: Float!
    updatedAt: Float!
  }

  input IdeaCreateInput {
    userId: Int!
    title: String!
    body: String
  }
  input IdeaUpdateInput {
    title: String!
    body: String
  }

  extend type User @key(fields: "id") {
    id: Int! @external
    ideas: [Idea]
  }

  extend type Query {
    ideas: [Idea]
    idea(id: Int!): Idea
  }

  extend type Mutation {
    createIdea(input: IdeaCreateInput!): Idea
    updateIdea(id: Int!, input: IdeaUpdateInput!): Idea
  }

`;

module.exports = {
  typeDefs
};