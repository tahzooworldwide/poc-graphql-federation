const { gql } = require('apollo-server');

const typeDefs = gql`

  type AuthResponse {
    token: String
    message: String
    ok: Boolean!
  }

  extend type Mutation {
    register(name: String!, email: String!, password: String!): AuthResponse
    login(email: String!, password: String!): AuthResponse
  }

`;

module.exports = {
  typeDefs
};