const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const signJWT = (subject) => {

  const tokenOptions = {
    algorithm: process.env.JWT_ALGO,
    subject: String(subject),
    expiresIn: process.env.JWT_EXPIRE
  }

  return jwt.sign({}, process.env.JWT_SECRET, tokenOptions);

}


const resolvers = {

  Mutation: {
    async register(_, { name, email, password }, { dataSources }) {
      const exists = await dataSources.auth.exists(email);
      if (exists > 0) {
        return {
          ok: false,
          message: "A users already exists with the provided email."
        }
      }
      const hashedPassword = await bcrypt.hash(password, 10);
      const result = await dataSources.auth.register(name, email, hashedPassword);

      if (!result) {
        return {
          ok: false,
          message: "Registration Failed."
        }
      }

      const token = signJWT(result.id)

      return {
        ok: true,
        token
      }
    },
    async login(_, { email, password }, { dataSources }) {
      const result = await dataSources.auth.getUserByEmail(email);
      if (!result) {
        return {
          ok: false,
          message: "No user exists that matches the provided credentials."
        }
      }

      const matchesHash = await bcrypt.compare(password, result.password);
      if (!matchesHash) {
        return {
          ok: false,
          message: "No user exists that matches the provided credentials."
        }
      }

      const token = signJWT(result.id)

      return {
        ok: true,
        token,
        message: "Authentication Successful."
      }
    }
  }
};


const referenceResolvers = null;


module.exports = {
  resolvers,
  referenceResolvers
}