const { shield } = require("graphql-shield");
const { isGuest } = require("../authorization-gates");


const guard = shield({
  Mutation: {
    login: isGuest,
    register: isGuest,
  }
});

module.exports = { guard };