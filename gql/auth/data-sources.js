const { MongoDataSource } = require('apollo-datasource-mongodb');

class AuthDataSource extends MongoDataSource {

  async nextId() {
    const result = await this.collection.find({}, { id: 1, _id: 0 }).sort({ id: -1 }).limit(1).toArray();
    console.log("nextId", result);
    return result.length ? parseInt(result[0].id + 1) : 1;
  }

  async exists(email) {
    return await this.collection.count({ email });
  }

  async register(name, email, password) {
    const id = await this.nextId();
    const createdAt = (new Date()).getTime();
    const proposedRecord = {
      id,
      name,
      email,
      password,
      createdAt,
      updatedAt: null
    }
    const result = await this.collection.insertOne(proposedRecord);
    const record = await this.collection.findOne({ _id: result.insertedId });
    return record;
  }

  async getUserByEmail(email) {
    return await this.collection.findOne({ email });
  }

}

module.exports = { AuthDataSource }