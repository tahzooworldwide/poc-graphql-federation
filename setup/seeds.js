const bcrypt = require('bcrypt');
const basePassphrase =  bcrypt.hashSync("pass1234", 10);

const seeds = {
  users: [
   {
     name: "Fletch",
     email:"fletchp@tahzoo.com",
     password: basePassphrase
   },
   {
     name: "Ivan",
     email:"ivanj@tahzoo.com",
     password: basePassphrase
   },
   {
     name: "Roland",
     email:"roland.muts@tahzoo.com",
     password: basePassphrase
   },
   {
     name: "Jamie",
     email:"jamiel@tahzoo.com",
     password: basePassphrase
   },
   {
     name: "Piti",
     email:"piti@tahzoo.com",
     password: basePassphrase
   },
  ]
};


module.exports = seeds;