require('dotenv').config();
const faker = require('faker');
const seeds = require('./seeds');
const { MongoClient } = require('mongodb');

// Connection URL
const url = process.env.MONGO_HOST;
const client = new MongoClient(url);
const database = process.env.MONGO_DATABASE_DATA;
//
// utilities
//

// current timestamp helper
const now = () => (new Date()).getTime();


// record id helper
const idGenerator = function* () {
  let count = 0;
  while (true) {
    yield ++count;
  }
}

// non-invasive fisher-yates/knuth shuffler
// clones input leaving the original array intact
function shuffle(array) {
  let a = [...array];
  let cIdx = a.length, rIdx;
  while (0 !== cIdx) {
    rIdx = Math.floor(Math.random() * cIdx);
    cIdx--;
    [a[cIdx], a[rIdx]] = [a[rIdx], a[cIdx]];
  }
  return a;
}

//
// record factories
//

const userFactory = (id, name, email, password) => {
  const timestamp = now();
  return {
    id,
    name,
    email,
    password,
    active: true,
    createdAt: timestamp,
    updatedAt: timestamp
  };
};

const ideaFactory = (id, userId) => {
  const timestamp = now();
  return {
    id,
    userId,
    title: faker.lorem.sentence(),
    body: faker.lorem.paragraphs(),
    createdAt: timestamp,
    updatedAt: timestamp
  };
};

const commentFactory = (id, ideaId, userId) => {
  return {
    id,
    ideaId,
    userId,
    body: faker.lorem.sentences(),
    createdAt: now(),
  };
};

const voteFactory = (ideaId, userId) => {
  return {
    ideaId,
    userId,
    createdAt: now(),
  };
};

//
// generate seed data
//

// users
const users = seeds.users.map(({ name, email, password }, idx) => {
  return userFactory(idx + 1, name, email, password,);
})

// ideas
const ideaIdGenerator = idGenerator();
const ideas = users.map(user => {
  const out = [];
  for (let i = 0; i < 5; i++) {
    out.push(ideaFactory(ideaIdGenerator.next().value, user.id));
  }
  return out;
}).flat();

// comments 
const commentIdGenerator = idGenerator();
const comments = ideas.map(idea => {
  const shuffledUsers = shuffle(users).slice(0, faker.datatype.number({ min: 0, max: users.length - 1 }));
  const out = [];
  for (const user of shuffledUsers) {
    if (idea.userId !== user.id) {
      out.push(commentFactory(commentIdGenerator.next().value, idea.id, user.id));
    }
  }
  return out;
}).flat();

const collections = [
  { name: 'users', seeds: users },
  { name: 'ideas', seeds: ideas },
  { name: 'comments', seeds: comments },
]


async function execute(dbClient, dbName, seeds) {
  console.group("Seeding Databases");
  await dbClient.connect()
  const db = await dbClient.db(dbName)
  for (const seed of seeds) {
    console.log(`Seeding ${seed.name}`);
    await db.collection(seed.name).insertMany(seed.seeds);
  }
  console.groupEnd();
  return 'Done Seeeding Database(s).';
}

execute(client, database, collections)
  .then(console.log)
  .catch(console.error)
  .finally(() => client.close())