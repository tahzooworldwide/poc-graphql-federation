require('dotenv').config();
const { MongoClient } = require('mongodb');
const schema = require('./schema');

// Connection URL
const url = process.env.MONGO_HOST;
const client = new MongoClient(url);

async function createMongoDB(dbClient, {name, collections}) {

  const db = await dbClient.db(name)
  console.group(`Database: ${name}`);
  for (const collection of collections ) {
    await db.createCollection(collection.name);
    console.log(`Colllection: ${collection.name}`);
  }
  console.groupEnd();
}

async function execute(dbClient, defs) {
  console.group("Create Database(s)")
  await dbClient.connect()
  for (const databaseDefinition of defs.databases ) {
    await createMongoDB(dbClient, databaseDefinition)
  }

  console.groupEnd();
  return 'Done Creating Database(s).';
}

execute(client, schema)
  .then(console.log)
  .catch(console.error)
  .finally(() => client.close())