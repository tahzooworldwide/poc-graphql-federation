require('dotenv').config();
const { MongoClient } = require('mongodb');
const schema = require('./schema');

// Connection URL
const url = process.env.MONGO_HOST;
const client = new MongoClient(url);

async function dropMongoDB(dbClient, {name}) {
  try {
    const db = await dbClient.db(name)
    await db.dropDatabase()
    console.log(`Database: ${name}`);
  } catch (e) {
    console.error(e.toString());
  }
}

async function execute(dbClient, defs) {
  console.group("Dropping Database(s)")
  await dbClient.connect()
  for (const databaseDefinition of defs.databases ) {
    await dropMongoDB(dbClient, databaseDefinition)
  }
  console.groupEnd();
  return 'Done Dropping Database(s).';
}

execute(client, schema)
  .then(console.log)
  .catch(console.error)
  .finally(() => client.close())