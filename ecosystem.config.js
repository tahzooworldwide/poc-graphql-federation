require('dotenv').config();

module.exports = {
  apps: [
    {
      name: 'users-gql',
      script: 'gql/users/server.js',
      watch: ['./gql/users'],
      env: {
        PORT: process.env.USERS_GQL_PORT,
        MONGO_HOST: process.env.MONGO_HOST,
        MONGO_DATABASE: process.env.MONGO_DATABASE_DATA,
        MONGO_COLLECTION: process.env.USERS_MONGO_COLLECTION,
      }
    },
    {
      name: 'ideas-gql',
      script: 'gql/ideas/server.js',
      watch: ['./gql/ideas'],
      env: {
        PORT: process.env.IDEAS_GQL_PORT,
        MONGO_HOST: process.env.MONGO_HOST,
        MONGO_DATABASE: process.env.MONGO_DATABASE_DATA,
        MONGO_COLLECTION: process.env.IDEAS_MONGO_COLLECTION,
      }
    },
    {
      name: 'comments-gql',
      script: 'gql/comments/server.js',
      watch: ['./gql/comments'],
      env: {
        PORT: process.env.COMMENTS_GQL_PORT,
        MONGO_HOST: process.env.MONGO_HOST,
        MONGO_DATABASE: process.env.MONGO_DATABASE_DATA,
        MONGO_COLLECTION: process.env.COMMENTS_MONGO_COLLECTION,
      }
    },
    {
      name: 'votes-gql',
      script: 'gql/votes/server.js',
      watch: ['./gql/votes'],
      env: {
        PORT: process.env.VOTES_GQL_PORT,
        MONGO_HOST: process.env.MONGO_HOST,
        MONGO_DATABASE: process.env.MONGO_DATABASE_DATA,
        MONGO_COLLECTION: process.env.VOTES_MONGO_COLLECTION,
      }
    },
    {
      name: 'auth-gql',
      script: 'gql/auth/server.js',
      watch: ['./gql/auth'],
      env: {
        PORT:process.env.AUTH_GQL_PORT,
        MONGO_HOST: process.env.MONGO_HOST,
        MONGO_DATABASE: process.env.MONGO_DATABASE_DATA,
        MONGO_COLLECTION: process.env.AUTH_MONGO_COLLECTION,
        JWT_ALGO: process.env.JWT_ALGO,
        JWT_SECRET: process.env.JWT_SECRET,
        JWT_EXPIRE: process.env.JWT_EXPIRE,
      }
    },
    {
      name: 'gateway',
      script: 'gateway/server.js',
      watch: ['./gateway','./gql'],
      env: {
        PORT: process.env.GATEWAY_PORT,
        JWT_ALGO: process.env.JWT_ALGO,
        JWT_SECRET: process.env.JWT_SECRET,
      }
    },
  ],
};
