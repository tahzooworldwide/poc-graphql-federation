const { ApolloGateway, RemoteGraphQLDataSource } = require('@apollo/gateway');
const { ApolloServer } = require("apollo-server-express");
const express = require("express");
const expressJwt = require("express-jwt");

async function startGatewayServer() {

  const gateway = new ApolloGateway({
    serviceList: [
      { name: 'users', url: 'http://localhost:4010' },
      { name: 'ideas', url: 'http://localhost:4020' },
      { name: 'comments', url: 'http://localhost:4030' },
      { name: 'votes', url: 'http://localhost:4040' },
      { name: 'auth', url: 'http://localhost:4050' },
    ],
    buildService({ name, url }) {
      return new RemoteGraphQLDataSource({
        url,
        willSendRequest({ request, context }) {
          request.http.headers.set(
            "user",
            context.user ? JSON.stringify(context.user) : null
          );
        }
      });
    }
  });
  
  const server = new ApolloServer({
    gateway,
    subscriptions: false,
    cors: true,
    context: ({ req }) => {
      const user = req.user || null;
      return { user };
    },
  });
  await server.start();
  const app = express();
  app.use(
    expressJwt({
      secret:  process.env.JWT_SECRET,
      algorithms: [process.env.JWT_ALGO],
      credentialsRequired: false
    })
  );
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
  server.applyMiddleware({ app });
  await new Promise(resolve => app.listen({ port: process.env.PORT }, resolve));
  console.log(`🚀 Server ready at http://localhost:${process.env.PORT}${server.graphqlPath}`);
}

startGatewayServer();
